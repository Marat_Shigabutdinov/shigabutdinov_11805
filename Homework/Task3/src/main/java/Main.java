import java.io.FileWriter;
import java.time.LocalDate;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Student> students = StudentCol.loadStudent();

        for (Student person : students) {
            if (!person.getCity().equals("Moscow") && ((LocalDate.now().getYear() - person.getYear().getYear()) > 20)) {
                try (FileWriter fw = new FileWriter("person.dat")) {
                    fw.write(person.toString() + "\n");
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        }
    }
}


