import java.time.LocalDate;
import java.util.Calendar;

public class Student {
    private Enum<Group> group;
    private String name;
    private String city;
    private LocalDate year;

    public Student(Enum<Group> group, String name, String city, LocalDate year) {
        this.group = group;
        this.name = name;
        this.city = city;
        this.year = year;
    }

    @Override
    public String toString() {
        return "Student{" +
                "group=" + group +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", year=" + year +
                '}';
    }

    public String getCity() {
        return city;
    }

    public Enum<Group> getGroup() {
        return group;
    }

    public void setGroup(Enum<Group> group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public LocalDate getYear() {
        return year;
    }

    public void setYear(LocalDate year) {
        this.year = year;
    }
}
