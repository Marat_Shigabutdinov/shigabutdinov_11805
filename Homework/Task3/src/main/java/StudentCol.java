import java.time.LocalDate;
import java.util.ArrayList;

public class StudentCol {
    public static ArrayList<Student> loadStudent(){
        ArrayList<Student> people = new ArrayList<Student>();
        people.add(new Student(Group.GROUP_1, "Marat", "Moscow", LocalDate.of(2000, 4, 10)));
        people.add(new Student(Group.GROUP_1, "Edgar", "Kazan", LocalDate.of(1990, 4, 18)));
        people.add(new Student(Group.GROUP_1, "Ruslan", "Ufa", LocalDate.of(2000, 5, 25)));
        return people;
    }
}
