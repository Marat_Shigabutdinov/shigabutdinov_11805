import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        double sum = 0, temp = 1;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        for (int k = 1; k <= n; k++) {
            temp *= 2d / k;
            sum += temp;
        }
        System.out.println(sum);
    }
}
