package model;

import game.Game;
import game.PlayingField;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class Snake {

    private static final int DOT_RADIUS = 8;
    private ArrayList<Point> body = new ArrayList<>();
    private Game game;

    private PlayingField playingField;
    private Direction direction = Direction.RIGHT;

    public Snake(PlayingField playingField, Point position, Game game) {
        this.playingField = playingField;
        this.game = game;
        body.add(position);
        for (int i = 1; i < 3; i++) {
            position = new Point(position.x - DOT_RADIUS * 2, position.y);
            body.add(position);
        }
    }

    public void setDirection(Direction direction) {
        boolean isDirectionToInside = this.direction == Direction.RIGHT && direction == Direction.LEFT;
        if (!isDirectionToInside)
            isDirectionToInside = this.direction == Direction.LEFT && direction == Direction.RIGHT;
        if (!isDirectionToInside) isDirectionToInside = this.direction == Direction.UP && direction == Direction.DOWN;
        if (!isDirectionToInside) isDirectionToInside = this.direction == Direction.DOWN && direction == Direction.UP;

        if (!isDirectionToInside) this.direction = direction;
    }

    public void move() {
        for (int i = body.size() - 1; i > 0; i--) {
            body.get(i).x = body.get(i - 1).x;
            body.get(i).y = body.get(i - 1).y;
        }
        Point head = body.get(0);
        if (direction == Direction.UP) {
            head.y = head.y - DOT_RADIUS * 2;
        } else if (direction == Direction.DOWN) {
            head.y = head.y + DOT_RADIUS * 2;
        } else if (direction == Direction.RIGHT) {
            head.x = head.x + DOT_RADIUS * 2;
        } else if (direction == Direction.LEFT) {
            head.x = head.x - DOT_RADIUS * 2;
        }

        for (int i = 1; i < body.size(); i++) {
            if (head.getX() == body.get(i).getX() && head.getY() == body.get(i).getY()) {
                game.gameOver(this);
            }
        }
    }

    public void grow() {
        Point prelast = body.get(body.size() - 2);
        Point last = body.get(body.size() - 1);

        Point newLast = new Point((int) last.getX(), (int) last.getY());
        if (prelast.getX() < last.getX()) {
            newLast.x += DOT_RADIUS * 2;
        } else if (prelast.getX() > last.getX()) {
            newLast.x -= DOT_RADIUS * 2;
        } else if (prelast.getY() < last.getY()) {
            newLast.y += DOT_RADIUS * 2;
        } else if (prelast.getY() > last.getY()) {
            newLast.y -= DOT_RADIUS * 2;
        }
        body.add(newLast);
    }

    // returns true if grew
    public void eatIfCan(List<Point> food) {
        Point head = body.get(0);
        int xMin = (int) (head.getX() - DOT_RADIUS);
        int xMax = (int) (head.getX() + DOT_RADIUS);
        int yMin = (int) (head.getY() - DOT_RADIUS);
        int yMax = (int) (head.getY() + DOT_RADIUS);
        for (Point point : food) {
            int foodXMin = (int) (point.getX() - DOT_RADIUS);
            int foodXMax = (int) (point.getX() + DOT_RADIUS);
            int foodYMin = (int) (point.getY() - DOT_RADIUS);
            int foodYMax = (int) (point.getY() + DOT_RADIUS);

            if (foodXMax >= xMin && foodXMin <= xMax && foodYMax >= yMin && foodYMin <= yMax) {
                grow();
                food.remove(point);
                game.server.sendMessage("/removeFood " + point.x + " " + point.y);
                break;
            }
        }
    }

    public List<Point> getBody() {
        return body;
    }

    public Point getHead() {
        return body.get(0);
    }

    public boolean collides(Snake other) {
        Point head = getHead();
        int xMin = (int) (head.getX() - DOT_RADIUS);
        int xMax = (int) (head.getX() + DOT_RADIUS);
        int yMin = (int) (head.getY() - DOT_RADIUS);
        int yMax = (int) (head.getY() + DOT_RADIUS);
        for (Point point : other.getBody()) {
            int bodyXMin = (int) (point.getX() - DOT_RADIUS);
            int bodyXMax = (int) (point.getX() + DOT_RADIUS);
            int bodyYMin = (int) (point.getY() - DOT_RADIUS);
            int bodyYMax = (int) (point.getY() + DOT_RADIUS);
            if (bodyXMax >= xMin && bodyXMin <= xMax && bodyYMax >= yMin && bodyYMin <= yMax) {
                return true;
            }
        }
        return false;
    }
}