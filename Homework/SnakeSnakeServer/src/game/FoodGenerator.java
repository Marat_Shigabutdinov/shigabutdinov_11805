package game;

import socket.Server;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class FoodGenerator extends Thread {
    private Server socket;
    private List<Point> food = new ArrayList<>();

    public FoodGenerator(Server socket) {
        super();
        this.socket = socket;

    }

    @Override
    public void run(){
        while (true) {
            int x = new Random().nextInt(1280);
            int y = new Random().nextInt(720);
            food.add(new Point(x, y));
            socket.sendMessage("/food " + x + " " + y);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Point> getFood() {
        return food;
    }
}