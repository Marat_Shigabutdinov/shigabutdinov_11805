package game;

import model.Snake;
import socket.Server;

import java.awt.*;
import java.util.List;

public class SnakeMover extends Thread {
    private List<Snake> snakes;
    private List<Point> food;
    private Server server;
    private Game game;

    public SnakeMover(List<Snake> snakes, List<Point> food, Server server, Game game) {
        this.snakes = snakes;
        this.food = food;
        this.server = server;
        this.game = game;
    }

    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < snakes.size(); i++) {
                snakes.get(i).move();
                border(snakes.get(i));
                snakeInTheSnake(snakes.get(i));
                snakes.get(i).eatIfCan(food);
                server.sendMessage("/body " + i + " " + generateSnakeString(snakes.get(i)));
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String generateSnakeString(Snake snake) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Point point : snake.getBody()) {
            stringBuilder.append(point.x).append(" ").append(point.y).append(" ");
        }
        return stringBuilder.toString();
    }

    public void border(Snake snake) {
        Point head = snake.getHead();
        if (head.getX() < 0 || head.getX() > 1280 || head.getY() < 0 || head.getY() > 720) {
            game.gameOver(snake);
        }
    }

    public void snakeInTheSnake(Snake snake) {
        for (int i = 0; i < snakes.size(); i++) {
            if (i != snakes.indexOf(snake) && snake.collides(snakes.get(i))) {
                game.gameOver(snake);
            }
        }
    }

}
