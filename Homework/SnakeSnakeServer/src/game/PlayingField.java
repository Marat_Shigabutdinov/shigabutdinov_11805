package game;

import model.Snake;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlayingField {

    public static final int WIDTH = 1280;
    public static final int HEIGHT = 720;

    private List<Snake> snakes = new ArrayList<>();
    private List<Point> food = new ArrayList<>();

}