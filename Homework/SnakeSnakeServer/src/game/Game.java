package game;

import model.Snake;
import socket.ClientHandler;
import socket.Server;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Game {
    public Server server;
    private PlayingField playingField;
    private FoodGenerator foodGenerator;
    private List<Snake> snakes = Collections.synchronizedList(new ArrayList<>());
    private List<ClientHandler> clientHandlers = new ArrayList<>();
    private SnakeMover snakeMover;

    public void start(Server server) {
        this.server = server;
        playingField = new PlayingField();
        foodGenerator = new FoodGenerator(server);
        snakeMover = new SnakeMover(snakes, foodGenerator.getFood(), server, this);

        server.sendMessage("/start");
        for (int i = 0; i < 2; i++) {
            int x = new Random().nextInt(720);
            int y = new Random().nextInt(480);
            Snake snake = new Snake(playingField, new Point(x, y), this);
            snakes.add(snake);
            clientHandlers.get(i).setSnake(snake);
        }
        foodGenerator.start();
        snakeMover.start();
    }

    public void appendPlayer(ClientHandler clientHandler) {
        clientHandler.sendCommand("" + clientHandlers.size());
        clientHandlers.add(clientHandler);
    }

    public void gameOver(Snake snake){
        int playerIndex = snakes.indexOf(snake);
        playerIndex = playerIndex == 0 ? 1 : 0;
        foodGenerator.interrupt();
        snakeMover.interrupt();
        server.sendMessage("/finish " + playerIndex);
        server.stop();
    }
}