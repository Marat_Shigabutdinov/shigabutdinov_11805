package socket;

import model.Direction;
import model.Snake;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ClientHandler extends Thread {
    private Socket clientSocket;
    private BufferedReader in;
    public PrintWriter out;
    private String nickname;
    private Snake snake;


    public ClientHandler(Socket clientSocket) throws IOException {
        this.clientSocket = clientSocket;
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
        sendCommand("/snake " + snake.getBody().get(0).x + " " + snake.getBody().get(0).y);
    }

    public void run() {
        try {
            nickname = in.readLine();
            String cmd;
            while ((cmd = in.readLine())!= null){
                Scanner scanner = new Scanner(cmd);
                cmd = scanner.next();
                if ("/changeDirection".equals(cmd)) {
                    snake.setDirection(Direction.valueOf(scanner.next()));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void sendCommand(String cmd) {
        out.println(cmd);
    }
}
