package socket;

import game.Game;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Server {
    private Game game;
    private List<ClientHandler> clients = new CopyOnWriteArrayList<>();
    private ServerSocket serverSocket;

    public static void main(String[] args) {
        Server server = new Server();
        server.start(4444);
    }

    private void start(int port) {
        game = new Game();
        try {
            serverSocket = new ServerSocket(port);
            for (int i = 0; i < 2; i++) {
                clients.add(new ClientHandler(serverSocket.accept()));
                clients.get(clients.size() - 1).start();
                game.appendPlayer(clients.get(clients.size() - 1));
            }
            game.start(this);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void stop() {
        for (ClientHandler handler : clients) handler.interrupt();
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




    public  void sendMessage(String str){
        for (ClientHandler client : clients) {
            client.out.println(str);
        }
    }
}