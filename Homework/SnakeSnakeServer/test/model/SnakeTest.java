package model;

import game.Game;
import game.PlayingField;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import socket.Server;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class SnakeTest {

    @InjectMocks
    private PlayingField playingField = mock(PlayingField.class);

    private Game game = new Game();

    private Snake snake, snake1;
    private Point food;
    private ArrayList<Point> foods = new ArrayList<>();

    @Before
    public void before() {
        snake = new Snake(playingField, new Point(0, 0), game);
    }

    @Test
    public void grow() {
        int size = snake.getBody().size();
        snake.grow();
        assertEquals(snake.getBody().size(), size + 1);
    }

    @Before
    public void createSnake(){
        snake = new Snake(playingField, new Point(0,0), game);
    }
    public void createOther(){
        snake1 = new Snake(playingField, new Point(0,0), game);
    }

    @Test
    public void collidesTest(){
        createSnake();
        createOther();
        assertTrue(snake.collides(snake1));
    }
    @Before
    public void beforeEat(){
        food = new Point(0,0);
        foods.add(food);

    }
    @Test
    public void eatIfCanTest(){
        Game game = new Game();
        int a = snake.getBody().size();
        snake.eatIfCan(foods);
        int b = snake.getBody().size();
       assertEquals(a, b);
    }
}