import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class MessagesFromFile {
    public static ArrayList<String> recordFromIMessages() throws IOException {
        FileReader fl = new FileReader("messages.txt");
        Scanner sc = new Scanner(fl);
        ArrayList<String> messages = new ArrayList<String>();
        String idUser = URLDespatchor.path.replaceAll("\\D+", "");
        String idUser1 = idUser.substring(1);
        String idUser2 = idUser.substring(2);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.contains(idUser1) || line.contains(idUser2)) {
                String[] fields = line.split("", 2);
                String id = fields[0];
                if (!id.equals(idUser)) {
                    messages.add(line);
                }
            }
        }
        return messages;
    }
    public static ArrayList<String> recordFromOMessages() throws IOException {
        FileReader fl = new FileReader("messages.txt");
        Scanner sc = new Scanner(fl);
        ArrayList<String> messages = new ArrayList<String>();
        String idUser = URLDespatchor.path.replaceAll("\\D+", "");
        String idUser1 = idUser.substring(1);
        String idUser2 = idUser.substring(2);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.contains(idUser1) || line.contains(idUser2)) {
                String[] fields = line.split("", 2);
                String id = fields[0];
                if (id.equals(idUser)) {
                    messages.add(line);
                }
            }
        }
        return messages;
    }

}
