import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MapFromUsers {
    public static Map<String, String> recorderInMap() throws IOException {
        FileReader fileReader = new FileReader("users.txt");
        Scanner sc = new Scanner(fileReader);
        Map<String, String> users = new HashMap<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] field = line.split(" ", 2);
            String id = field[0];
            String fields = field[1];
            users.put(id, fields);
        }
        fileReader.close();
        return users;
    }
}