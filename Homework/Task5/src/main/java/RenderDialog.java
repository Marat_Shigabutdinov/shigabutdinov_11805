import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class RenderDialog {
    public static void showDialog() throws IOException {
        ArrayList<String> dialog1 = MessagesFromFile.recordFromIMessages();
        ArrayList<String> dialog2 = MessagesFromFile.recordFromOMessages();
        Map<String,String > users = MapFromUsers.recorderInMap();

        FileWriter fl = new FileWriter("Dialog.html");
        for (int i = 0; i < dialog1.size(); i++) {
            String[] message = dialog1.get(i).split(" ");
            String id = message[0];
            String text = message[1];
            String imId = message[2];
            String[] param = users.get(id).split(" ", 2);
            String name = param[0];
            fl.write("<html><head><title>" + "messages" + "</title></head><body><h1>" +
                    name + "<h1><h1>" +
                    text + "<h1></body></html>");
        }
        for (int i = 0; i < dialog2.size(); i++) {
            String[] message = dialog2.get(i).split(" ");
            String id = message[0];
            String text = message[1];
            String imId = message[2];
            String[] param = users.get(imId).split(" ", 2);
            String name = param[0];
            fl.write("<html><head><title>" + "messages" + "</title></head><body><h1>" +
                    name + "<h1><h1>" +
                    text + "<h1></body></html>");
        }
        fl.close();
    }
}

