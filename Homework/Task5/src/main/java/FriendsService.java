import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class FriendsService {
    public static ArrayList<String> getFriends() throws IOException {
        ArrayList<String> friendsList = FriendFromSubscription.recordFromSubscription();
        ArrayList<String> friends = new ArrayList<String>();
        Map<String, String> users = MapFromUsers.recorderInMap();
        for (int i = 0; i < friendsList.size(); i++) {
            String friend = friendsList.get(i);
            friends.add(users.get(friend));
        }
        return friends;
    }
}
