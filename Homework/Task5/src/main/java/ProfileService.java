import java.io.IOException;
import java.util.Map;

public class ProfileService {
    public static String getProfile(String path) throws IOException {
        String str = path;
        Map<String, String> users = MapFromUsers.recorderInMap();
        String id = str.replaceAll("\\D+", "");
        return users.get(id);
    }
}