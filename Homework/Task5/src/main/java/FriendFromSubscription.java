import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class FriendFromSubscription {
    public static ArrayList<String> recordFromSubscription() throws IOException {
        String str = URLDespatchor.path;
        Map<String, String> users = MapFromUsers.recorderInMap();
        FileReader fl = new FileReader("subscription.txt");
        Scanner scanner = new Scanner(fl);
        ArrayList<String> friends = new ArrayList<String>();
        String idUser = str.replaceAll("\\D+", "");
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.contains(idUser)) {
                String[] fields = line.split("", 2);
                String id = fields[0];
                if (!id.equals(idUser)) {
                    friends.add(id);
                }
            }
        }
        return friends;
    }
}
