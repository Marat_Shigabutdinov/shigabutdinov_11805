import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class RenderUserWithFriends {
    public static void showUserWithFriends() throws IOException {
        String user = ProfileService.getProfile(URLDespatchor.path);
        ArrayList<String> friends = FriendsService.getFriends();

        if (!friends.isEmpty()) {
            FileWriter fl = new FileWriter("userWithFriends.html");
            String[] param = user.split(" ");
            String name = param[0];
            String year = param[1];
            String city = param[2];

            fl.write("<html><head><title>" + name + "</title></head><body><h1>" +
                    name + "<h1><h1>" +
                    year + "<h1><h1>" +
                    city + "<h1>");

            for (int i = 0; i < friends.size(); i++) {
                String fieldFriend = friends.get(i);
                String[] paramFriend = fieldFriend.split(" ");
                String nameFriend = paramFriend[0];
                String yearFriend = paramFriend[1];
                String cityFriend = paramFriend[2];

                fl.write("<h1>" +
                        nameFriend + "<h1><h1>" +
                        yearFriend + "<h1><h1>" +
                        cityFriend + "<h1></body>");

            }
            fl.close();
        }
    }
}