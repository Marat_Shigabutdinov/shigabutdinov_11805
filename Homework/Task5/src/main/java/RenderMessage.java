import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

public class RenderMessage {
    public static void showImMessages() throws IOException {
        ArrayList<String> messages = MessagesFromFile.recordFromIMessages();
        Map<String, String > users = MapFromUsers.recorderInMap();

        FileWriter fl = new FileWriter("ImMessages.html");
        for (int i = 0; i < messages.size(); i++) {
            String[] message = messages.get(i).split(" ");
            String id  = message[0];
            String text = message[1];
            String imId = message[2];
            String[] param = users.get(id).split(" ", 2);
            String name = param[0];
            fl.write("<html><head><title>" + "messages" + "</title></head><body><h1>" +
                    name + "<h1><h1>" +
                    text + "<h1></body></html>");
        }
        fl.close();
    }

    public static void showOmMessages() throws IOException {
        ArrayList<String> messages = MessagesFromFile.recordFromOMessages();
        Map<String, String > users = MapFromUsers.recorderInMap();

        FileWriter fl = new FileWriter("OmMessages.html");
        for (int i = 0; i < messages.size(); i++) {
            String[] message = messages.get(i).split(" ");
            String id  = message[0];
            String text = message[1];
            String imId = message[2];
            String[] param = users.get(imId).split(" ", 2);
            String name = param[0];
            fl.write("<html><head><title>" + "messages" + "</title></head><body><h1>" +
                    name + "<h1><h1>" +
                    text + "<h1></body></html>");
        }
        fl.close();
    }
}
