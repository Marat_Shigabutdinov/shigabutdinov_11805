import java.io.IOException;
import java.util.Scanner;

public class URLDespatchor {
    private static Scanner scanner = new Scanner(System.in);
    static String path = scanner.nextLine();


    public static void dispatchor(String path) throws IOException {
        while (true) {
            if (path.contains("/profile?id=")) {
                if (path.contains("&friends=true")) {
                    RenderUserWithFriends.showUserWithFriends();
                } else {
                    RenderUser.showUser();
                }
            } else if (path.contains("/friends?id=")) {
                RenderFriends.showFriends();
            } else if (path.contains("/im?id=")) {
                RenderMessage.showImMessages();
            } else if (path.contains("/om?id=")) {
                RenderMessage.showOmMessages();
            } else if(path.contains("/messages?id=")){
                RenderDialog.showDialog();
            } else {

            }
        }
    }

    public static void main(String[] args) throws IOException {
        dispatchor(path);
    }
}
