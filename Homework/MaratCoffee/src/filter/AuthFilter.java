package filter;

import model.Pages;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) {
        if (needAuth(req)) {
            try {
                chain.doFilter(req, resp);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                resp.sendRedirect(Pages.MAIN);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
