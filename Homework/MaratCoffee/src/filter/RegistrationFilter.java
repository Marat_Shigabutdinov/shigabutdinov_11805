package filter;

import model.Pages;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static model.Parameters.*;

public class RegistrationFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain){
        if (needAuth(req)) {
            boolean isPost = "post".equals(req.getMethod());
            if (isPost) {
                if (checkParams(req, NAME, SURNAME, EMAIL, PASSWORD, CONFIRM, TABLE, REMEMBER_ME)
                        && req.getParameter(PASSWORD).equals(req.getParameter(CONFIRM))) {
                    try {
                        chain.doFilter(req, resp);
                    } catch (IOException | ServletException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        chain.doFilter(req, resp);
                    } catch (IOException | ServletException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    resp.sendRedirect(Pages.RESTORE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

