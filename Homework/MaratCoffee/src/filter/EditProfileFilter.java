package filter;

import model.Pages;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static model.Parameters.*;

public class EditProfileFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) {
        boolean isPost = "post".equals(req.getMethod());
        if (!isPost || checkParams(req, NAME, SURNAME, EMAIL, TABLE)) {
            try {
                chain.doFilter(req, resp);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        } else {
            try {
                resp.sendRedirect(Pages.EDIT_PROFILE);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}