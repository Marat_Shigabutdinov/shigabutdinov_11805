package filter;

import model.Pages;
import model.Parameters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProfileFilter extends HttpFilter {
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain){
        if (checkParams(req, Parameters.ID) || !needAuth(req)) {
            try {
                chain.doFilter(req, resp);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        } else {
            try {
                resp.sendRedirect(Pages.LOGIN);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}