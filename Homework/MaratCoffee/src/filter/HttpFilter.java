package filter;

import model.SessionAttributes;
import service.AuthService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

abstract class HttpFilter implements javax.servlet.Filter {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain){
        try {
            doFilter(
                    (HttpServletRequest) req,
                    (HttpServletResponse) resp,
                    chain
            );
        } catch (ServletException | IOException e) {
            e.printStackTrace();
        }
    }

    protected abstract void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain)
            throws ServletException, IOException;

    boolean checkParams(HttpServletRequest request, String... params) {
        for (String param : params) {
            if (request.getParameter(param) == null) return false;
        }

        return true;
    }

    boolean needAuth(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object o = session.getAttribute(SessionAttributes.CURRENT_USER_ID);
        boolean hasSession = o != null && (int) o != -1;
        return !hasSession && AuthService.get().tryAuth(request) == -1;
    }
}
