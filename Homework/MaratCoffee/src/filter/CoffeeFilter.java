package filter;

import model.Parameters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CoffeeFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain){
        if (checkParams(req, Parameters.ID)) {
            try {
                chain.doFilter(req, resp);
            } catch (IOException | ServletException e) {
                e.printStackTrace();
            }
        }
    }
}

