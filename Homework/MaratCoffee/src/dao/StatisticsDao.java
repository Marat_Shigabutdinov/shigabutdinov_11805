package dao;

import model.Statistic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

public class StatisticsDao extends AbstractDao<Statistic> {
    private static final String TABLE = "statistics";

    public StatisticsDao() {
        super(TABLE);
    }

    @Override
    protected Statistic parseEntityFrom(ResultSet resultSet) throws SQLException {
        Calendar date = Calendar.getInstance();
        date.setTime(resultSet.getDate(7));

        return new Statistic(
                resultSet.getInt(1),
                resultSet.getInt(2),
                resultSet.getInt(3),
                resultSet.getInt(4),
                resultSet.getInt(5),
                resultSet.getInt(6),
                date
        );
    }
}
