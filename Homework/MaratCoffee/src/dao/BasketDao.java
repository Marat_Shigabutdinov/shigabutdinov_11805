package dao;

import model.BasketProduct;
import model.Coffee;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BasketDao extends AbstractDao<BasketProduct> {
    private static final String TABLE = "basket";

    public BasketDao() {
        super(TABLE);
    }

    @Override
    protected BasketProduct parseEntityFrom(ResultSet resultSet) throws SQLException {
        Coffee coffee = new Coffee(
                resultSet.getInt(3),
                resultSet.getString(4),
                resultSet.getString(5),
                resultSet.getDouble(6),
                resultSet.getInt(7)
        );

        return new BasketProduct(
                resultSet.getInt(1),
                resultSet.getInt(2),
                coffee
        );
    }
}
