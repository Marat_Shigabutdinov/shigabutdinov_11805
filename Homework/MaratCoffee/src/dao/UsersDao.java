package dao;

import model.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsersDao extends AbstractDao<User> {
    private static final String TABLE = "users";

    public UsersDao() {
        super(TABLE);
    }

    @Override
    protected User parseEntityFrom(ResultSet resultSet) throws SQLException {
        return new User(
                resultSet.getInt(1),
                resultSet.getString(2),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5),
                resultSet.getInt(6)
        );
    }
}
