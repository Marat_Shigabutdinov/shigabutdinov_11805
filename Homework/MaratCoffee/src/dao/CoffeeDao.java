package dao;

import model.Coffee;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CoffeeDao extends AbstractDao<Coffee> {
    private static final String TABLE = "coffee";

    public CoffeeDao() {
        super(TABLE);
    }

    @Override
    protected Coffee parseEntityFrom(ResultSet resultSet) throws SQLException {
        return new Coffee(
                resultSet.getInt(1),
                resultSet.getString(2),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getDouble(5),
                resultSet.getDouble(6),
                resultSet.getInt(7),
                resultSet.getInt(8)
        );
    }

    public List<Coffee> search(String query) {
        String sql = "SELECT * FROM postgres.public." + TABLE + " WHERE title LIKE ?";

       List<Coffee> coffees = new ArrayList<>();
       try(PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + query + "%");
           ResultSet resultSet = statement.executeQuery();
           while (resultSet.next()){
               coffees.add(parseEntityFrom(resultSet));
           }
       } catch (SQLException e){
           e.printStackTrace();
       }
        return coffees;
    }
}
