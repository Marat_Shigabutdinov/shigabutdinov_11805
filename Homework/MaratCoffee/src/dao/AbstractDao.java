package dao;

import model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractDao<E extends Entity> {
    public Connection connection;
    private String table;

    AbstractDao(String table) {
        connection = ConnectionPool.getConnection();
        this.table = "coffee." + table;
    }

    protected abstract E parseEntityFrom(ResultSet resultSet) throws SQLException;

    public boolean insert(E entity) {
        boolean modified = false;

        try {
            String sql = "INSERT INTO " + table + " VALUES (";
            String[] values = entity.getValues();
            sql += "?";
            for (int i = 1; i < values.length; i++) {
                sql += ", ?";
            }
            sql += ")";

            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i = 0; i < values.length; i++) statement.setString(i + 1, values[i]);
            modified = statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return modified;
    }

    public E getEntityById(int id) {
        String sql = "SELECT * FROM " + table + " WHERE id = ?";
        return getAll().stream().filter(coffee -> coffee.getId() == id).collect(Collectors.toList()).get(0);
    }

    public E getEntityWith(String[] columns, String[] values) {
        List<E> result = getEntitiesWith(columns, values);
        return result.isEmpty() ? null : result.get(0);
    }

    public List<E> getEntitiesWith(String[] columns, String[] values) {
        String sql = "SELECT * FROM " + table + " WHERE " + columns[0] + " = ?";
        for (int i = 1; i < columns.length; i++) {
            sql += " AND " + columns[i] + " = ?";
        }

        return executeQuery(sql, values);
    }

    public List<E> getEntities(String whereClause, String... whereArgs) {
        String sql = "SELECT * FROM " + table + " WHERE " + whereClause;

        return executeQuery(sql, whereArgs);
    }

    public List<E> getAll() {
        String sql = "SELECT * FROM " + table;

        return executeQuery(sql, new String[0]);
    }

    public int update(E entity) {
        String[] fields = entity.getFields();
        String sql = "UPDATE " + table + " SET " + fields[0] + " = ?";
        for (int i = 1; i < fields.length; i++) {
            sql += ", " + fields[i] + " = ?";
        }
        sql += " WHERE id = ?";

        String[] values1 = entity.getValues();
        String[] values2 = new String[values1.length + 1];
        for (int i = 0; i < entity.getValues().length; i++) {
            values2[i] = entity.getValues()[i];
        }
        values2[values2.length - 1] = Integer.toString(entity.getId());

        return executeUpdate(sql, values2);
    }

    public boolean delete(int id) {
        String sql = "DELETE FROM" + table + " WHERE id = ?";

        return execute(sql, new String[]{Integer.toString(id)});
    }

    private boolean execute(String sql, String[] values) {
        boolean executed = false;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setString(i + 1, values[i]);
            }

            executed = statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return executed;
    }

    List<E> executeQuery(String sql, String[] values) {
        List<E> entityList = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setString(i + 1, values[i]);
            }
            System.out.println(statement);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) entityList.add(parseEntityFrom(resultSet));
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entityList;
    }

    private int executeUpdate(String sql, String[] values) {
        int result = 0;
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            for (int i = 0; i < values.length; i++) {
                statement.setString(i + 1, values[i]);
            }
            result = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
}