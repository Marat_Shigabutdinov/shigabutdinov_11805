package dao;

import model.Feedback;
import service.UserService;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FeedbackDao extends AbstractDao<Feedback> {
    private static final String TABLE = "feedback";

    private UserService userService = UserService.get();

    public FeedbackDao() {
        super(TABLE);
    }

    @Override
    protected Feedback parseEntityFrom(ResultSet resultSet) throws SQLException {
        return new Feedback(
                resultSet.getInt(1),
                userService.getUserById(resultSet.getInt(2)),
                resultSet.getString(3),
                resultSet.getInt(4)
        );
    }
}
