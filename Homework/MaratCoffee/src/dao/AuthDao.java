package dao;

import model.AuthData;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class AuthDao extends AbstractDao<AuthData> {
    private static final String TABLE = "auth";

    public AuthDao() {
        super(TABLE);
    }

    @Override
    protected AuthData parseEntityFrom(ResultSet resultSet) throws SQLException {
        return new AuthData(
                resultSet.getInt(1),
                resultSet.getString(2),
                resultSet.getString(3),
                resultSet.getInt(4),
                resultSet.getString(5)
        );
    }

    public AuthData getEntityByLogin(String login) {
        String sql = "SELECT * FROM  coffee." +  TABLE + " WHERE email = ?";

        List<AuthData> result = executeQuery(sql, new String []{login});
        return result.isEmpty() ? null : result.get(0);
    }
}
