package model;

public class Parameters {
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String CONFIRM = "confirm";
    public static final String TABLE = "table";
    public static final String REMEMBER_ME = "rememberMe";
    public static final String LOGIN = "login";
    public static final String CODE = "code";
    public static final String ID = "id";
    public static final String TEXT = "text";
    public static final String SUGAR_COUNT = "sugarCount";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String QUERY = "query";
    public static final String RATING = "rating";
}
