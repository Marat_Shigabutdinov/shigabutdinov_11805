package model;

import java.util.Calendar;

public class Comment extends Entity {
    private int coffeeId;
    private User commentator;
    private String text;
    private Calendar date;

    public Comment(User commentator, String text) {
        this.commentator = commentator;
        this.text = text;
    }

    public Comment(int id, int coffeeId, User commentator, String text, Calendar date) {
        this(commentator, text);
        this.id = id;
        this.coffeeId = coffeeId;
        this.date = date;
    }

    public int getCoffeeId() {
        return coffeeId;
    }

    public User getCommentator() {
        return commentator;
    }

    public String getText() {
        return text;
    }

    public Calendar getDate() {
        return date;
    }

    @Override
    public String[] getFields() {
        return new String[]{
                "id",
                "coffeeId",
                "commentatorId",
                "text",
                "date"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(coffeeId),
                Integer.toString(commentator.getId()),
                text,
                date.toString()
        };
    }
}
