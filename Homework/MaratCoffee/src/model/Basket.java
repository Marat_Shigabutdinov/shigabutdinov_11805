package model;

import java.util.List;

public class Basket {
    private List<BasketProduct> products;

    public Basket(List<BasketProduct> products) {
        this.products = products;
    }

    public List<BasketProduct> getProducts() {
        return products;
    }

    public double getPrice() {
        double price = 0;
        for (BasketProduct product : products) price += product.get().getPrice();

        return price;
    }
}
