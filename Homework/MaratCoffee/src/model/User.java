package model;

public class User extends Entity {
    private String name, surname;
    private String email;
    private String photoSrc;
    private int table;

    public User(String name, String surname, String email, int table) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.table = table;
    }

    public User(int id, String name, String surname, String email, String photoSrc, int table) {
        this(name, surname, email, table);
        this.id = id;
        this.photoSrc = photoSrc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotoSrc() {
        return photoSrc;
    }

    public void setPhotoSrc(String photoSrc) {
        this.photoSrc = photoSrc;
    }

    public int getTable() {
        return table;
    }

    public void setTable(int table) {
        this.table = table;
    }

    @Override
    public String toString() {
        return name + " " + surname;
    }

    @Override
    public String[] getFields() {

        return new String[]{
                "id",
                "name",
                "surname",
                "email",
                "photoSrc",
                "tabl"};
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                name,
                surname,
                email,
                photoSrc,
                Integer.toString(table)
        };
    }
}
