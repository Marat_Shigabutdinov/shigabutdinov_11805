package model;

public class BasketProduct extends Entity {
    private int buyerId;
    private Coffee product;

    public BasketProduct(int buyerId, Coffee product) {
        this.buyerId = buyerId;
        this.product = product;
    }

    public BasketProduct(int id, int buyerId, Coffee product) {
        this(buyerId, product);
        this.id = id;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public Coffee get() {
        return product;
    }

    @Override
    public String[] getFields() {
        return new String[] {
                "id",
                "buyerId",
                "productId",
                "productTitle",
                "productImageSrc",
                "productPrice",
                "productSugarCount"
        };
    }

    @Override
    public String[] getValues() {
        return new String[] {
                Integer.toString(id),
                Integer.toString(buyerId),
                Integer.toString(product.getId()),
                product.getTitle(),
                product.getImageSrc(),
                Double.toString(product.getPrice()),
                Integer.toString(product.getSugarCount())
        };
    }
}
