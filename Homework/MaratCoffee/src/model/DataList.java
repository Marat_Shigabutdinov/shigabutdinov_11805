package model;

import java.util.Collection;

public class DataList<T> {
    private Collection<T> data;

    public DataList(Collection<T> data) {
        this.data = data;
    }
}
