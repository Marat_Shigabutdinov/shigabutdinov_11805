package model;

public class Feedback extends Entity {
    private User user;
    private String text;
    private int rates;

    public Feedback(User user, String text) {
        this.user = user;
        this.text = text;
    }

    public Feedback(int id, User user, String text, int rates) {
        this(user, text);
        this.id = id;
        this.rates = rates;
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public int getRates() {
        return rates;
    }

    public void increaseRates() {
        rates++;
    }

    @Override
    public String[] getFields() {
        return new String[]{
                "id",
                "userId",
                "text",
                "rates"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(user.getId()),
                text,
                Integer.toString(rates)
        };
    }
}
