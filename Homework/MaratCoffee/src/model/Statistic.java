package model;

import java.util.Calendar;

public class Statistic extends Entity {
    private int forDay;
    private int forWeek;
    private int forMonth;
    private int forYear;
    private int forAllTime;

    public Statistic(int id, int forDay, int forWeek, int forMonth, int forYear, int forAllTime, Calendar date) {
        this.id = id;

        Calendar now = Calendar.getInstance();
        if (date.get(Calendar.DAY_OF_MONTH) == now.get(Calendar.DAY_OF_MONTH)) {
            this.forDay = forDay;
        }
        if (date.get(Calendar.WEEK_OF_MONTH) == now.get(Calendar.WEEK_OF_MONTH)) {
            this.forWeek = forWeek;
        }
        if (date.get(Calendar.MONTH) == now.get(Calendar.MONTH)) {
            this.forMonth = forMonth;
        }
        if (date.get(Calendar.YEAR) == now.get(Calendar.YEAR)) {
            this.forYear = forYear;
        }
        this.forAllTime = forAllTime;
    }

    public int getForDay() {
        return forDay;
    }

    public int getForWeek() {
        return forWeek;
    }

    public int getForMonth() {
        return forMonth;
    }

    public int getForYear() {
        return forYear;
    }

    public int getForAllTime() {
        return forAllTime;
    }

    public void add(int count) {
        forDay += count;
        forWeek += count;
        forMonth += count;
        forYear += count;
        forAllTime += count;
    }

    @Override
    public String[] getFields() {
        return new String[] {
                "id",
                "forDay",
                "forWeek",
                "forMonth",
                "forYear",
                "forAllTime"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                Integer.toString(forDay),
                Integer.toString(forWeek),
                Integer.toString(forMonth),
                Integer.toString(forYear),
                Integer.toString(forAllTime)
        };
    }
}
