package model;

public class Coffee extends Entity {
    private String title;
    private String imageSrc;
    private String description;
    private double price;
    private double rating;
    private int ratesCount;
    private int sugarCount;

    public Coffee(int id, String title, String imageSrc, String description, double price, double rating, int ratesCount, int sugarCount) {
        this.id = id;
        this.title = title;
        this.imageSrc = imageSrc;
        this.description = description;
        this.price = price;
        this.rating = rating;
        this.ratesCount = ratesCount;
        this.sugarCount = sugarCount;
    }

    public Coffee(int id, String title, String imageSrc, double price, int sugarCount) {
        this.id = id;
        this.title = title;
        this.imageSrc = imageSrc;
        this.price = price;
        this.sugarCount = sugarCount;
    }

    public String getTitle() {
        return title;
    }

    public String getImageSrc() {
        return imageSrc;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public void setRating(double rating, int ratesCount) {
        this.rating = rating;
        this.ratesCount = ratesCount;
    }

    public void rate(double rating) {
        this.rating = (this.rating * ratesCount++ + rating) / ratesCount;
    }

    public int getSugarCount() {
        return sugarCount;
    }

    public void setSugarCount(int count) {
        sugarCount = count;
    }

    @Override
    public String[] getFields() {
        return new String[]{
                "id",
                "title",
                "imagesrc",
                "description",
                "price",
                "rating",
                "ratescount",
                "sugarcount"
        };
    }

    @Override
    public String[] getValues() {
        return new String[]{
                Integer.toString(id),
                title,
                imageSrc,
                description,
                Double.toString(price),
                Double.toString(rating),
                Integer.toString(ratesCount),
                Integer.toString(sugarCount)
        };
    }
}
