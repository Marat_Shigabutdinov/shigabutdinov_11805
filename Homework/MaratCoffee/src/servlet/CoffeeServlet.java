package servlet;

import freemarker.Render;
import model.Coffee;
import model.Parameters;
import service.CoffeeService;
import service.SessionService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CoffeeServlet extends HttpServlet {
    private CoffeeService service = CoffeeService.get();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Map<String, Object> data = new HashMap<>(2);

        System.out.println(service.getAvailableCoffees().get(0));

        data.put("userId", SessionService.get().getCurrentUserId(request));
        Coffee coffee = service.getCoffeeById(
                Integer.parseInt(request.getParameter(Parameters.ID))
        );
        data.put("coffee", coffee);
        data.put("coffees", service.getAvailableCoffees());
        Render.rend(request, writer, "coffee.ftl", data);
        writer.close();
    }
}
