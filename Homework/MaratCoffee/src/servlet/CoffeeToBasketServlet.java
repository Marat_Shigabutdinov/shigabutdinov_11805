package servlet;

import freemarker.Render;
import model.Basket;
import model.BasketProduct;
import model.Coffee;
import model.Parameters;
import service.BasketService;
import service.CoffeeService;
import service.SessionService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class CoffeeToBasketServlet extends HttpServlet {
    private CoffeeService coffeeService = CoffeeService.get();
    private BasketService basketService = BasketService.get();

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        Coffee coffee = coffeeService.getCoffeeById(
                Integer.parseInt(request.getParameter(Parameters.ID))
        );
        coffee.setSugarCount(
                Integer.parseInt(request.getParameter(Parameters.SUGAR_COUNT))
        );
        BasketProduct product = new BasketProduct(
                SessionService.get().getCurrentUserId(request),
                coffee
        );

        basketService.addProduct(product);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Coffee coffee = coffeeService.getCoffeeById(
                Integer.parseInt(request.getParameter(Parameters.ID))
        );

        Map<String, Object> data = new HashMap<>(1);
        data.put(request.getParameter(Parameters.ID), coffee);

        Render.rend(request, writer, "coffee_to_basket.ftl", data);
        writer.close();
    }
}
