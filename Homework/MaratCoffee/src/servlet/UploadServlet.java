package servlet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;

public abstract class UploadServlet extends HttpServlet {
    String getPathTo(String localDir) {
        return getServletContext().getRealPath("") + File.separator + localDir;
    }

    String getFileExtension(String fileName) {
        String[] fileNameData = fileName.split("\\.");
        return fileNameData[fileNameData.length - 1];
    }

    void uploadFile(Part filePart, String dest, String fileName) throws IOException {
        File dir = new File(dest);
        if (!dir.exists()) {
            dir.mkdir();
        }

        filePart.write(dest + File.separator + fileName);
    }
}
