package servlet;

import freemarker.Render;
import model.*;
import service.FeedbackService;
import service.SessionService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class FeedbackServlet extends HttpServlet {
    private UserService userService = UserService.get();
    private FeedbackService feedbackService = FeedbackService.get();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = userService.getUserById(SessionService.get().getCurrentUserId(req));
        feedbackService.addFeedback(new Feedback(
                user,
                req.getParameter(Parameters.TEXT)
        ));
        resp.sendRedirect(Pages.FEEDBACK);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Map<String, Object> data =  new HashMap<>(1);
        data.put("feedback", feedbackService.getFeedback());

        Render.rend(request, writer, "feedback.ftl", data);
        writer.close();
    }
}
