package servlet;

import freemarker.Render;
import model.Parameters;
import model.User;
import service.SessionService;
import service.UserService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class ProfileServlet extends HttpServlet {
    private UserService service = UserService.get();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        int id = SessionService.get().getCurrentUserId(request);
        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", id);
        String sid = request.getParameter(Parameters.ID);
        id = sid == null ? id : Integer.parseInt(sid);
        User profile = service.getUserById(id);
        data.put("user", profile);

        Render.rend(request,writer,"profile.ftl", data);
        writer.close();
    }
}
