package servlet;

import freemarker.Render;
import service.CoffeeService;
import service.SessionService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class MainServlet extends HttpServlet {
    private CoffeeService service = CoffeeService.get();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", SessionService.get().getCurrentUserId(request));
        data.put("coffees", service.getAvailableCoffees());
        Render.rend(request, writer, "main.ftl", data);
        writer.close();
    }
}
