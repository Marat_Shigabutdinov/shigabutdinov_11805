package servlet;

import freemarker.Render;
import model.*;
import service.SessionService;
import service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class EditProfileServlet extends UploadServlet {
    private UserService service = UserService.get();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User user = service.getUserById(SessionService.get().getCurrentUserId(request));
        user.setName(request.getParameter(Parameters.NAME));
        user.setSurname(request.getParameter(Parameters.SURNAME));
        user.setEmail(request.getParameter(Parameters.EMAIL));
        String photoPath = uploadUserPhoto(request);
        user.setPhotoSrc(photoPath);
        user.setTable(
                Integer.parseInt(request.getParameter(Parameters.TABLE))
        );
        service.updateUser(user);

        response.sendRedirect(Pages.PROFILE);
    }

    private String uploadUserPhoto(HttpServletRequest request) throws ServletException, IOException {
        Part part = request.getPart("userPhoto");
        String dest = getPathTo(LocalDirs.USER_PHOTOS);
        String fileName = SessionService.get().getCurrentUserId(request) + getFileExtension(part.getSubmittedFileName());
        uploadFile(part, dest, fileName);

        return dest + File.separator + fileName;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        User user = service.getUserById(SessionService.get().getCurrentUserId(request));
        Map<String, Object> data = new HashMap<>(1);
        data.put("user", user);

        Render.rend(request, writer, "profile_edit.ftl", data);
        writer.close();
    }
}
