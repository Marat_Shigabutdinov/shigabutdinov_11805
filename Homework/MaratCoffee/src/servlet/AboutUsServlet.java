package servlet;

import freemarker.Render;
import service.SessionService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class AboutUsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Map<String, Object> data = new HashMap<>(1);
        data.put("userId", SessionService.get().getCurrentUserId(request));

        Render.rend(request, writer,"about_us.ftl", data);
        writer.close();
    }
}
