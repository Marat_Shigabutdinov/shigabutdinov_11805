package servlet;

import model.Parameters;
import service.AuthService;
import service.EmailService;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SendCodeServlet extends HttpServlet {
    private AuthService authService = AuthService.get();
    private EmailService emailService = EmailService.get();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String email = req.getParameter(Parameters.EMAIL);
        sendCode(email, authService.generateRecoveryCode(email));
    }

    private void sendCode(String email, String code) {
        try {
            emailService.sendMessage(
                    email,
                    "Password recovery code",
                    "Your code is " + code
            );
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
