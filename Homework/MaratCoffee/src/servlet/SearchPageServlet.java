package servlet;

import freemarker.Render;
import model.Parameters;
import service.SessionService;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class SearchPageServlet extends HttpServlet {
    private SessionService sessionService = SessionService.get();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        String query = request.getParameter(Parameters.QUERY);

        Map<String, Object> data = new HashMap<>(2);
        data.put("userId", sessionService.getCurrentUserId(request));
        data.put("query", query);

        System.out.println("Search rabotai");

        Render.rend(request, writer, "search.ftl", data);
    }
}