package servlet;

import freemarker.Render;
import model.*;

import service.RegistrationService;
import service.SessionService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.Cookie;

public class RegistrationServlet extends HttpServlet {
    private RegistrationService service = RegistrationService.get();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        User user = new User(
                request.getParameter(Parameters.NAME),
                request.getParameter(Parameters.SURNAME),
                request.getParameter(Parameters.EMAIL),
                Integer.parseInt(request.getParameter(Parameters.TABLE))
        );
        String password = request.getParameter(Parameters.PASSWORD);

        int id = service.register(user, password);
        if (id > 0) {
            response.addCookie(new Cookie (CookieAttributes.USER_ID, Integer.toString(id)));
            response.sendRedirect(Pages.MAIN);
        } else response.sendRedirect(Pages.REGISTRATION);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        Map<String, Object> data = new HashMap<>(1);
        data.put("userId",  SessionService.get().getCurrentUserId(request));

        Render.rend(request, writer, "registration.ftl", data);
        writer.close();
    }

}
