package servlet;

import model.Parameters;
import service.CoffeeService;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RateCoffeeServlet extends HttpServlet {
    private CoffeeService service = CoffeeService.get();

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        service.rateCoffee(
                Integer.parseInt(request.getParameter(Parameters.ID)),
                Integer.parseInt(request.getParameter(Parameters.RATING))
        );
    }
}
