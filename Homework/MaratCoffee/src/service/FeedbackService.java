package service;

import dao.FeedbackDao;
import model.Feedback;

import java.util.List;

public class FeedbackService {
    private static FeedbackService instance;

    private FeedbackDao dao;

    private FeedbackService() {
        dao = new FeedbackDao();
    }

    public static FeedbackService get() {
        if (instance == null) instance = new FeedbackService();

        return instance;
    }

    public List<Feedback> getFeedback() {
        return dao.getAll();
    }

    public void addFeedback(Feedback feedback) {
        dao.insert(feedback);
    }
}
