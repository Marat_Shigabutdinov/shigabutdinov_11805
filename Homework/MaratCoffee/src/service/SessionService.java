package service;

import model.SessionAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionService {
    private static SessionService instance;

    private AuthService authService;

    public static SessionService get() {
        if (instance == null) instance = new SessionService();

        return instance;
    }

    private SessionService() {
        authService = AuthService.get();
    }

    public int getCurrentUserId(HttpServletRequest request) {
        int id;

        HttpSession session = request.getSession();
        if (session.getAttribute(SessionAttributes.CURRENT_USER_ID) != null) {
            id = (int) session.getAttribute(SessionAttributes.CURRENT_USER_ID);
        } else id = authService.tryAuth(request);

        return id;
    }
}
