package service;

import model.User;


public class RegistrationService {
    private static RegistrationService instance;

    private AuthService authService;
    private UserService userService;

    private RegistrationService() {
        authService = AuthService.get();
        userService = UserService.get();
    }

    public static RegistrationService get() {
        if (instance == null) instance = new RegistrationService();

        return instance;
    }

    public int register(User user, String password) {
        if (authService.isLoginFree(user.getEmail())) {
            return -1;
        }

        authService.addAuthData(user.getEmail(), password);
        userService.addUser(user);

        return userService.getUserByEmail(user.getEmail()).getId();
    }
}
