package service;

import dao.BasketDao;
import model.Basket;
import model.BasketProduct;

public class BasketService {
    private static BasketService instance;

    private BasketDao dao;

    private BasketService() {
        dao = new BasketDao();
    }

    public static BasketService get() {
        if (instance == null) instance = new BasketService();

        return instance;
    }

    public Basket getBasketForUser(int userId) {
        return new Basket(
                dao.getEntitiesWith(
                new String[]{"buyerId"},
                new String[]{Integer.toString(userId)}
                )
        );
    }

    public void addProduct(BasketProduct product) {
        dao.insert(product);
    }
}
