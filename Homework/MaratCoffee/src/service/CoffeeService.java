package service;

import dao.CoffeeDao;
import model.Coffee;

import java.util.List;

public class CoffeeService {
    private static CoffeeService instance;

    private CoffeeDao dao;

    private CoffeeService() {
        dao = new CoffeeDao();
    }

    public static CoffeeService get() {
        if (instance == null) instance = new CoffeeService();

        return instance;
    }

    public List<Coffee> search(String query) {
        return dao.search(query);
    }

    public List<Coffee> getAvailableCoffees() {
        return dao.getAll();
    }

    public Coffee getCoffeeById(int id) {
        return dao.getEntityById(id);
    }

    public void rateCoffee(int id, double rating) {
        Coffee coffee = dao.getEntityById(id);
        coffee.rate(rating);
        dao.update(coffee);
    }
}
