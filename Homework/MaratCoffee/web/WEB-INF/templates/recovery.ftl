<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./static/join.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Restore</title>
</head>
<style>
    body {
        background-color: #fbfbfb;
    }


    .company-name {
        font-family: 'Ubuntu', sans-serif;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        color: #0B2349;
        display: table;
        font-size: 60px;
        font-weight: 700;
        text-align: center;
        position: absolute;
        left: 38.5%;
        top: 15px;
    }

    .join-form {
        background: #fbfbfb;
        width: 400px;
        height: 460px;
        border-radius: 5px;
        position: absolute;
        left: 38.5%;
        top: 250px;
        box-shadow: 0 0 14px 2px #b9b9b9;
        padding: 20px;
        text-align: center;
    }

    .login-form {
        width: 400px;
        height: 450px;
    }

    .form-control {
        transition: none !important;
    }

    .form-control:active,
    .form-control:focus
    {
        box-shadow: 0 0 7px 4px #e4e4e4;
        border: 1px solid #ced4da;
    }

    .btn-primary {
        width: 100px;
        color: #fbfbfb;
        background-color: #a5a5a5 !important;
        border: none;
        margin-bottom: 20px;
        margin-top: 20px;
    }

    .btn-primary:hover,
    .btn-primary:focus,
    .btn-primary:active {
        width: 100px;
        color: #fbfbfb;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        border: none !important;
        margin-bottom: 20px;
        margin-top: 20px;
        box-shadow: 0 0 10px 1px #d47033 !important;
    }


    .btn-primary[type="restore"] {
        width: 100px;
        height: calc(1.5em + .75rem);
        position: absolute;
        right: 20px;
        top: 131px;
    }

    #code {
        width: 70%;
    }

    .form-group {
        font-family: 'Ubuntu', sans-serif;
        text-align: center;
        margin-top: 30px;
    }

    a {
        color: #a5a5a5;
    }
    a:hover {
        color: #000;
    }
</style>
<body>
<div class="company-name">
    Marat's Coffee
</div>
<div class="join-form">
    <h3>Join Us</h3>
    <form method="POST">
        <div class="form-group">
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" id="code" placeholder="Code">
        </div>
        <button type="restore" class="btn btn-primary">Get Code</button>
        <div class="form-group">
            <input type="password" class="form-control" id="password" placeholder="New Password">
        </div>
        <button type="submit" class="btn btn-primary">Restore</button>
    </form>
    <hr align="center" color="#5c2e19" width="80%">
    <a href="/login">Already have an account?</a>
</div>
</body>
</html>
