<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/main.css">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Marat's Coffee</title>
</head>
<style>
    body {
        background-color: #eee;
    }

    h1 {
        text-align: center;
    }

    .background {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        background-color: rgba(0,0,0,.3);
        background-blend-mode: darken;
        z-index: -1;
    }

    .navbar {
        box-shadow: 0 0 10px 4px #464646;
    }

    .logout {
        position: absolute;
        right: 1%;
    }
</style>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Marat's Coffee</a>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="/">Home</a>
            <a class="nav-item nav-link" href="/profile/productbasket/new">Add Coffee</a>
            <a class="nav-item nav-link" href="/about">About Us</a>
            <a class="nav-item nav-link" href="/feedback">Feedback</a>
            <form class="search-form" action="/search">
                <input type="search" class="nav-item search" id="search" placeholder="Search">
            </form>
            <#if userId == -1>
                <a class="nav-item nav-link" href="/join">Registration</a>
                <a class="nav-item nav-link" href="/login">Log In</a>
            <#else>
                <a class="nav-item nav-link logout" href="/">Logout</a>
            </#if>
        </div>
    </div>
</nav>
</body>
</html>