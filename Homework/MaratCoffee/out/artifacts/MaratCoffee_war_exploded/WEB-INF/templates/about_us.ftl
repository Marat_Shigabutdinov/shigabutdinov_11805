<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/about_us.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>About Us</title>
</head>
<style>
    body {
        background-color: #eee;
    }

    h1 {
        text-align: center;
    }

    input {
        background: transparent;
        color: black;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: solid 2px #ddd;
    }

    .background {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        background-color: rgba(0,0,0,.3);
        background-blend-mode: darken;
    }

    .navbar {
        box-shadow: 0 0 10px 4px #464646;
    }

    .logout {
        position: absolute;
        right: 1%;
    }

    .data-name {
        text-align: left;
    }

    .profile {
        width: 1600px;
        height: 800px;
        background-color: #fbfbfb;
        position: absolute;
        top: 10%;
        left: 8%;
        border-radius: 5px;
        box-shadow: 0 0 10px 4px #464646;
        padding: 40px;
    }
</style>
<body>
<div class="background"></div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">Marat's Coffee</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="/">Home</a>
            <a class="nav-item nav-link" href="/profile">Profile</a>
            <a class="nav-item nav-link" href="/profile/productbasket/new">Add coffee</a>
            <a class="nav-item nav-link active" href="/about">About Us</a>
            <a class="nav-item nav-link logout" href="/logout">Logout</a>
        </div>
    </div>
</nav>
<div class="profile">
    <h1 class="data-name">About Us</h1>
    <br>
    <p>William Shakespeare, often called England's national poet, is considered the greatest dramatist of all time. His works are loved throughout the world, but Shakespeare's personal life is shrouded in mystery.</p>
</div>
</body>
</html>
