<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/home.css" type="text/css">
    <link rel="stylesheet" href="/css/main.css" type="text/css">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Marat's Coffee</title>
</head>
<style>
    body {
        background-color: #eee;
    }

    h1 {
        text-align: center;
    }

    .background {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        background-color: rgba(0,0,0,.3);
        background-blend-mode: darken;
        z-index: -1;
    }

    .navbar {
        box-shadow: 0 0 10px 4px #464646;
    }

    .logout {
        position: absolute;
        right: 1%;
    }

    h1 {
        text-align: left !important;
    }

    .company-name,
    .company-name:hover {
        font-family: 'Ubuntu', sans-serif;
        color: white;
        font-size: 20px;
        text-decoration: none;
        position: absolute;
        left: 20px;
        top: 20px;
    }

    .search {
        position: relative;
        left: 20px;
        color: #000;
        background: transparent;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: solid 2px #ccc;
        width: 70px;
        height: 40px;
        border-radius: 0;
        outline: none;
    }

    .search:focus {
        width: 200px;
        animation: .2s linear search-width;
    }

    .grid-item {
        background-color: #fbfbfb;
        border-radius: 5px;
        width: 300px;
        height: 360px;
        margin-bottom: 30px;
        padding: 20px;
    }

    .grid-item img {
        width: 260px;
        height: 260px;
        border-radius: 3px;
        margin-bottom: 20px;
    }

    a, a:hover {
        color: black;
    }

    .grid-item:hover {
        box-shadow: 0 0 18px 2px #1f1f1f;
        animation: .15s linear coffee;
    }


    .grid {
        position: absolute;
        width: 1500px;
        top: 80px;
        left: 15%;
    }

    @keyframes coffee {
        0% {
            box-shadow: 0 0 8px 1px #1f1f1f;
        }
        25% {
            box-shadow: 0 0 12px 1px #1f1f1f;
        }
        50% {
            box-shadow: 0 0 14px 1px #1f1f1f;
        }
        75% {
            box-shadow: 0 0 15px 2px #1f1f1f;
        }
        100% {
            box-shadow: 0 0 17px 2px #1f1f1f;
        }
    }

    @keyframes search-width {
        0% {
            width: 70px;
        }
        25% {
            width: 100px;
        }
        50% {
            width: 130px;
        }
        75% {
            width: 160px;
        }
        100% {
            width: 200px;
        }
    }

</style>
<body>
<#include "menu.ftl">
<div class="grid" data-masonry='{ "itemSelector": ".grid-item", "columnWidth": 100, "gutter": 10 }'>
    <#list coffees as coffee>
    <a class="coffee-link" href="/coffee?id=${coffee.id}">
        <div class="grid-item">
            <img src="${coffee.imageSrc}" alt="">
            <h2>${coffee.title} </h2>
        </div>
    </a>
    </#list>
</div>
</body>
</html>
