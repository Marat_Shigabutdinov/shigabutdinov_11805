<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/profile.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>Profile ${user.name}</title>
</head>
<style>
    input {
        background: transparent;
        color: black;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: solid 2px #ddd;
    }

    .data-name {
        text-align: left;
    }

    .profile {
        width: 1100px;
        height: 800px;
        background-color: #fbfbfb;
        position: absolute;
        top: 10%;
        left: 8%;
        border-radius: 5px;
        box-shadow: 0 0 10px 4px #464646;
        padding: 40px;
    }

    .profile-pic img {
        width: 320px;
        height: 320px;
        border-radius: 5px;
        position: absolute;
        top: 120px;
    }

    .profile-info {
        position: absolute;
        left: 50%;
        top: 120px;
        font-size: 26px;
    }

    .profile-table {
        width: 400px;
        height: 800px;
        background-color: #fbfbfb;
        position: absolute;
        top: 10%;
        right: 8%;
        border-radius: 5px;
        box-shadow: 0 0 10px 4px #464646;
        padding: 25px;
    }

    .coffee-table {
        font-size: 26px;
        text-align: center;
        position: absolute;
    }

    .coffee-list {
        padding: 15px;
        background-color: white;
        border-radius: 5px;
        border: solid 2px #ddd;
        width: 350px;
        height: 650px;
        overflow: scroll;
    }

    .coffee {
        font-size: 18px;
        text-align: left;
    }

    a#edit {
        width: 320px;
        position: absolute;
        left: 40px;
        top: 470px;
    }

    a#review {
        position: absolute;
        top: 520px;
        left: 40px;
        width: 320px;
    }

    button#save {
        position: absolute;
        top: 470px;
        width: 320px;
        left: 50%;
    }

    .btn-primary {
        color: #353535;
        background-color: #ddd;
        border: none;
    }

    .btn-primary:hover,
    .btn-primary:active,
    .btn-primary:focus,
    .btn-primary:not(:disabled):not(.disabled).active,
    .btn-primary:not(:disabled):not(.disabled):active,
    .show>.btn-primary.dropdown-toggle {
        color: #353535;
        background-color: #ddd;
        border: none !important;
        box-shadow: 0 0 7px 1px #9c9c9c !important;
        border: none;
    }
</style>
<body>
<div class="background"></div>
<#include "menu.ftl">
<div class="profile">
    <h1 class="data-name">Your information</h1>
    <br>
    <div class="profile-pic">
        <img src=${user.photoSrc} alt="">
    </div>
    <div class="profile-info">
        <table cellpadding="10">
            <tr>
                <td>Name:</td>
                <td><input type="text" name="name" value="${user.name}" disabled></input></td>
            </tr>
            <tr>
                <td>E-mail:</td>
                <td><input type="text" name="name" value="${user.email}" disabled></input></td>
            </tr>
            <tr>
                <td>Table:</td>
                <td><input type="text" name="name" value="${user.table}" disabled></input></td>
            </tr>
        </table>
    </div>
    <a href="/profile/edit" class="btn btn-primary" id="edit">Edit profile</a>
    <a href="" class="btn btn-primary" id="review">Write a review</a>
</div>
</body>
</html>
