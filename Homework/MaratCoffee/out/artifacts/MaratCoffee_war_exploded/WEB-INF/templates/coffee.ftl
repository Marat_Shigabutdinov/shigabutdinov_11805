<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/coffee.css">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <title>${coffee.title}</title>
</head>
<style>
    body {
        background-color: #eee;
    }

    h1 {
        text-align: center;
    }

    input {
        background: transparent;
        color: black;
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: solid 2px #ddd;
    }

    .background {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        background-color: rgba(0,0,0,.3);
        background-blend-mode: darken;
    }

    .navbar {
        box-shadow: 0 0 10px 4px #464646;
    }

    .logout {
        position: absolute;
        right: 1%;
    }

    .data-name {
        text-align: left;
    }

    .profile {
        width: 1100px;
        height: 800px;
        background-color: #fbfbfb;
        position: absolute;
        top: 10%;
        left: 8%;
        border-radius: 5px;
        box-shadow: 0 0 10px 4px #464646;
        padding: 40px;
    }

    .coffee-pic img {
        width: 500px;
        height: 500px;
        border-radius: 5px;
        position: absolute;
        top: 120px;
    }

    .coffee-info {
        position: absolute;
        left: 52%;
        top: 120px;
        font-size: 26px;
    }

    .profile-table {
        width: 400px;
        height: 800px;
        background-color: #fbfbfb;
        position: absolute;
        top: 10%;
        right: 8%;
        border-radius: 5px;
        box-shadow: 0 0 10px 4px #464646;
        padding: 25px;
    }

    .coffee-table {
        font-size: 26px;
        text-align: center;
        position: absolute;
    }

    .coffee-list {
        padding: 15px;
        background-color: white;
        border-radius: 5px;
        border: solid 2px #ddd;
        width: 350px;
        height: 650px;
        overflow: scroll;
    }

    .coffee {
        font-size: 18px;
        text-align: left;
    }

    button#add {
        position: absolute;
        top: 700px;
        width: 320px;
        right: 8%;
    }

    .btn-primary {
        color: #353535;
        background-color: #ddd;
        border: none;
    }

    .btn-primary:hover,
    .btn-primary:active,
    .btn-primary:focus,
    .btn-primary:not(:disabled):not(.disabled).active,
    .btn-primary:not(:disabled):not(.disabled):active,
    .show>.btn-primary.dropdown-toggle {
        color: #353535;
        background-color: #ddd;
        border: none !important;
        box-shadow: 0 0 7px 1px #9c9c9c !important;
        border: none;
    }
</style>
<style>
    body {
        background-color: #eee;
    }

    h1 {
        text-align: center;
    }

    .background {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
        background: linear-gradient(135deg, rgba(184,61,0,1) 0%, rgba(233,103,32,1) 35%, rgba(0,212,255,1) 100%);
        background-color: rgba(0,0,0,.3);
        background-blend-mode: darken;
        z-index: -1;
    }

    .navbar {
        box-shadow: 0 0 10px 4px #464646;
    }

    .logout {
        position: absolute;
        right: 1%;
    }

</style>
<body>
<div class="background"></div>
<#include "menu.ftl">
<div class="profile">
    <h1 class="data-name">${coffee.title}</h1>
    <br>
    <div class="coffee-pic">
        <img src="${coffee.imageSrc}" alt="">
    </div>
    <div class="coffee-info">
        <table cellpadding="10">
            <tr>
                <td>Price:</td>
                <td><input type="text" name="name" value="${coffee.price}" disabled></input></td>
            </tr>
            <tr>
                <td>Rating:</td>
                <td><input type="text" name="name" value="4,6" disabled></td>
            </tr>
        </table>
        <p style="position: absolute; left: 10px; top: 150px;">${coffee.description}</p>
    </div>
    <button href="/profile/productbasket/new?id=${coffee.id}" class="btn btn-primary" id="add">Add to cart</button>
</div>
<div class="profile-table">
    <div class="coffee-table">
        <h1>Your table</h1>
        <br>
        <div class="coffee-list">
            <#list coffees as coffee>
                <div class="coffee">
                    <a href="/coffee?id=${coffee.id}">${coffee.title}</a>
                    <p>${coffee.description}</p>
                </div>
                <hr color="#ddd" width="100%">
            </#list>
        </div>
    </div>
</div>
</body>
</html>