import java.util.List;

public class Page {
    private int id;
    private String title;
    private List<User> subscribers;

    public Page(int id, String title, List<User> subscribers) {
        this.id = id;
        this.title = title;
        this.subscribers = subscribers;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<User> getSubscribers() {
        return subscribers;
    }
}
