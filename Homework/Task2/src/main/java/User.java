import java.util.ArrayList;
import java.util.List;

public class User extends Page {
    private int id;
    private String name;
    private String surname;
    private int age;

    public User(int id, String name, String surname, int age) {
        // new ArrayList is an emulation of subscribers
        super(id, name + " " + surname, new ArrayList<>());
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }
}
