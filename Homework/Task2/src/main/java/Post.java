import java.awt.*;
import java.util.Calendar;

public class Post {
    private int user_id;
    private Calendar date;
    private int likes;
    private String message;
    private Image art;

    public Post(int user_id, Calendar date, int likes, String message, Image art) {
        this.user_id = user_id;
        this.date = date;
        this.likes = likes;
        this.message = message;
        this.art = art;
    }

    public int getUser_id() {
        return user_id;
    }

    public Calendar getDate() {
        return date;
    }

    public int getLikes() {
        return likes;
    }

    public String getMessage() {
        return message;
    }

    public Image getArt() {
        return art;
    }
}
