public class Messages {
    private int id_sender;
    private int id_receiver;
    private String text;

    public Messages(int id_sender, int id_receiver, String text) {
        this.id_sender = id_sender;
        this.id_receiver = id_receiver;
        this.text = text;
    }

    public int getId_sender() {
        return id_sender;
    }

    public int getId_receiver() {
        return id_receiver;
    }

    public String getText() {
        return text;
    }
}
