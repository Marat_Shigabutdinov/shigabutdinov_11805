import java.awt.*;
import java.util.Calendar;

public class Photo {
    private int id;
    private Calendar date;
    private int likes;
    private Image art;

    public Photo(int id, Calendar date, int likes, Image art) {
        this.id = id;
        this.date = date;
        this.likes = likes;
        this.art = art;
    }

    public int getId() {
        return id;
    }

    public Calendar getDate() {
        return date;
    }

    public int getLikes() {
        return likes;
    }

    public Image getArt() {
        return art;
    }
}
