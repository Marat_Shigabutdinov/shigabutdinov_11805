package game;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(MockitoJUnitRunner.class)
class PlayingFieldTest {
    @InjectMocks
    PlayingField playingField = new PlayingField(720, 480);
    private Point point;

    @Test
    public void addFood() {
        point = new Point(0,0);
        int a = playingField.getChildren().size();
        playingField.addFood(this.point);
        int b = playingField.getChildren().size();
        assertEquals(a + 1, b);
    }
}