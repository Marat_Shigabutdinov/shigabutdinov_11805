package ui.ingameView;

import ui.helpers.PlayerSettings;
import ui.helpers.SceneSetterHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.IOException;

public class IngameViewConrtoller {


    private SceneSetterHelper sceneSetterHelper = new SceneSetterHelper();

    @FXML
    private ImageView ingameViewBackgroundImageView;

    @FXML
    void ingameViewQuitButtonPressed(ActionEvent event) throws IOException {
        sceneSetterHelper.setScene("mainMenu");
    }

    @FXML
    private void initialize() {
        ingameViewBackgroundImageView.setImage(new Image(PlayerSettings.getBackgroundImage()));
    }
}


