package ui.mainMenu;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ui.helpers.WindowSettingsHelper;

public class MainMenuMain extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        Stage nickNameChooser = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/ui/mainMenu/ip_setter/ip.fxml"));
        Parent root = loader.load();
        nickNameChooser.setResizable(false);
        nickNameChooser.setScene(new Scene(root));
        nickNameChooser = WindowSettingsHelper.configure(nickNameChooser, "Nick Name Chooser");
        nickNameChooser.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
