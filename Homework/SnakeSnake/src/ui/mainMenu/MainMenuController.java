package ui.mainMenu;

import ui.helpers.SceneSetterHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import java.io.IOException;

public class MainMenuController {

    private String playerNickName;
    private SceneSetterHelper sceneSetterHelper = new SceneSetterHelper();
    public static Stage primaryStage;

    @FXML
    private Button mainMenuPlayButton;

    @FXML
    private Button mainMenuSettingsButton;

    @FXML
    private Button mainMenuQuitButton;

    @FXML
    private Label mainMenuHelloTextLabel;

    @FXML
    void mainMenuPlayButtonPressed(ActionEvent event) throws IOException {
        sceneSetterHelper.setScene("gameModeChooser");
    }

    @FXML
    void mainMenuSettingsButtonPressed(ActionEvent event) throws IOException {
        sceneSetterHelper.setScene("settings");
    }

    @FXML
    void mainMenuQuitButtonPressed(ActionEvent event) {
        mainMenuQuitButton.getScene().getWindow().hide();
    }

    public void configure(String playerNickName) {
        this.playerNickName = playerNickName;
        mainMenuHelloTextLabel.setText(mainMenuHelloTextLabel.getText() + playerNickName);
    }

    @FXML
    void mainMenuLeaderboardButtonPressed(ActionEvent event) throws IOException {
        sceneSetterHelper.setScene("leaderboard");
    }
}

