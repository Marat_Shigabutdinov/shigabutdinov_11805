package ui.mainMenu.preconnection;

import ui.helpers.SceneSetterHelper;
import game.Game;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;

public class ConnectionController {

    private SceneSetterHelper sceneSetterHelper = new SceneSetterHelper();



    @FXML
    private Button gameModeChooserMultiplayerButton;

    @FXML
    private Button gameModeChooserBackButton;

    @FXML
    void gameModeChooserBackButtonPressed(ActionEvent event) {
        gameModeChooserBackButton.getScene().getWindow().hide();
    }

    @FXML
    void gameModeChooserMultiplayerButtonPressed(ActionEvent event) throws IOException {
        SceneSetterHelper sceneSetterHelper = new SceneSetterHelper();
        gameModeChooserMultiplayerButton.getScene().getWindow().hide();
        sceneSetterHelper.setScene("ingameView");
        Game game = new Game();
        game.start(sceneSetterHelper);
    }



}

