package ui.mainMenu.ip_setter;

import ui.helpers.PlayerSettings;
import ui.helpers.ShakingHelper;
import ui.helpers.WindowSettingsHelper;
import ui.mainMenu.MainMenuController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class SetIpController {

    @FXML
    private Button nickNameChooserQuitButton;

    @FXML
    private TextField nickNameChooserTextField;

    @FXML
    private Button nickNameChooserEnterButton;

    @FXML
    void nickNameChooserEnterButtonPressed(ActionEvent event) throws IOException {
        if (!nickNameChooserTextField.getText().equals("")) {

            if (nickNameChooserTextField.getText().length() > 15) {
                ShakingHelper shakingHelper = new ShakingHelper();
                shakingHelper.shakeStage((Stage) nickNameChooserTextField.getScene().getWindow());
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("Error: Ip lenght");
                alert.setContentText("Ip cannot be more than 15 characters");
                nickNameChooserTextField.setText("");
                alert.showAndWait();
            } else {
                PlayerSettings.setServerName(nickNameChooserTextField.getText());
                MainMenuController.primaryStage = new Stage();
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/ui/mainMenu/mainMenu.fxml"));
                Parent scene = loader.load();
                MainMenuController.primaryStage.setScene(new Scene(scene));
                MainMenuController.primaryStage = WindowSettingsHelper.configure(MainMenuController.primaryStage);
                MainMenuController controller = loader.getController();
                controller.configure(nickNameChooserTextField.getText());
                nickNameChooserTextField.getScene().getWindow().hide();
                MainMenuController.primaryStage.show();
            }
        }
    }

    @FXML
    void nickNameChooserQuitButtonPressed(ActionEvent event) {
        nickNameChooserQuitButton.getScene().getWindow().hide();
    }

}

