package ui.settings;

import ui.helpers.PlayerSettings;
import ui.helpers.SceneSetterHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;

import java.io.IOException;

public class SettingsController {

    private SceneSetterHelper sceneSetterHelper = new SceneSetterHelper();

    @FXML
    private Button settingsBackToMainMenuButton;

    @FXML
    private RadioButton settingsRadioButtonLeft;

    @FXML
    private ToggleGroup radioButton;

    @FXML
    private RadioButton settingsRadioButtonRight;

    @FXML
    void settingsBackToMainMenuButtonPressed(ActionEvent event) throws IOException {
        sceneSetterHelper.setScene("mainMenu");
    }

    @FXML
    void settingsRadioButtonLeftPressed(ActionEvent event) {
        PlayerSettings.setBackgroundImage("/ui/globalAssets/tekstury-oboi-texture-92a8c9b.jpg");
    }

    @FXML
    void settingsRadioButtonRightPressed(ActionEvent event) {
        PlayerSettings.setBackgroundImage("/ui/globalAssets/5a41ceca37782160910fc5f3.jpg");
    }

}

