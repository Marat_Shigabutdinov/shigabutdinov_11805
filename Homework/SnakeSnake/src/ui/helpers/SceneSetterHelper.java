package ui.helpers;

import ui.mainMenu.MainMenuController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class SceneSetterHelper {

    private WindowSettingsHelper settingsHelper = new WindowSettingsHelper();
    private Stage primaryStage = MainMenuController.primaryStage;

    public SceneSetterHelper() {}

    public void setScene(Scene scene, String title) {
        primaryStage.setScene(scene);
        primaryStage.setTitle(title);
    }

    public void setScene(String scene) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        switch (scene) {
            case "mainMenu":
                loader.setLocation(getClass().getResource("/ui/mainMenu/mainMenu.fxml"));
                Parent mainMenu = loader.load();
                MainMenuController controller = loader.getController();
                controller.configure(PlayerSettings.getServerName());
                Scene mainMenuScene = new Scene(mainMenu);
                primaryStage.setResizable(false);
                primaryStage.setScene(mainMenuScene);
                primaryStage = WindowSettingsHelper.configure(primaryStage);
                break;

            case "gameModeChooser":
                loader.setLocation(getClass().getResource("/ui/mainMenu/preconnection/connection.fxml"));
                Parent gameModeChooser = loader.load();
                Stage gameModeChooserStage = new Stage();
                gameModeChooserStage.initModality(Modality.APPLICATION_MODAL);
                gameModeChooserStage.setResizable(false);
                gameModeChooserStage.setScene(new Scene(gameModeChooser));
                gameModeChooserStage = settingsHelper.configure(gameModeChooserStage, "Game Mode Chooser");
                gameModeChooserStage.showAndWait();
                break;

            case "ingameView":
                loader.setLocation(getClass().getResource("/ui/ingameView/ingameView.fxml"));
                Parent ingameView = loader.load();
                Scene ingameViewScene = new Scene(ingameView);
                primaryStage.setResizable(false);
                primaryStage.setScene(ingameViewScene);
                primaryStage = settingsHelper.configure(primaryStage);
                break;

            case "settings":
                loader.setLocation(getClass().getResource("/ui/settings/settings.fxml"));
                Parent settings = loader.load();
                Scene settingsScene = new Scene(settings);
                primaryStage.setResizable(false);
                primaryStage.setScene(settingsScene);
                primaryStage = settingsHelper.configure(primaryStage);
                break;
        }
    }
}
