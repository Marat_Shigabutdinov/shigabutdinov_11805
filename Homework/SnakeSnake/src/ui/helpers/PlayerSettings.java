package ui.helpers;

public class PlayerSettings {

    private static String serverName;
    private static String backgroundImage = "/ui/globalAssets/5a41ceca37782160910fc5f3.jpg";

    public static String getServerName() {
        return serverName;
    }

    public static void setServerName(String serverName) {
        PlayerSettings.serverName = serverName;
    }

    public static String getBackgroundImage() {
        return backgroundImage;
    }

    public static void setBackgroundImage(String backgroundImage) {
        PlayerSettings.backgroundImage = backgroundImage;
    }



}
