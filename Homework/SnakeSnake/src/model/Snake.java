package model;

import game.PlayingField;
import javafx.scene.paint.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


public class Snake  {

    private static final int DOT_RADIUS = 8;
    ArrayList <Dot> body = new ArrayList<>();

    private PlayingField playingField;
    private Direction direction = Direction.RIGHT;

    public Snake(PlayingField playingField) {
        this.playingField = playingField;
    }

    public Snake(PlayingField playingField, List<Point> points) {
        this.playingField = playingField;
        Dot dot = new Dot(playingField, points.get(0), DOT_RADIUS);
        dot.setFill(Color.BLUE);
        body.add(dot);
        for (int i = 1; i < points.size(); i++) {
            Point point = new Point(points.get(i).x - DOT_RADIUS * 2, points.get(i).y);
            dot = new Dot(playingField, point, DOT_RADIUS);
            dot.setFill(Color.YELLOW);
            body.add(dot);
        }
        playingField.getChildren().addAll(body);
    }

    public void setBody(List<Point> points){
        for (int i = 0; i < body.size() && i < points.size(); i++) {
            body.get(i).setCenterX(points.get(i).getX());
            body.get(i).setCenterY(points.get(i).getY());
        }
        int k = body.size() - 1;
        if (k < 0) k = 0;
        for (int i = k; i < points.size() - 1; i++) {
            Dot dot = new Dot(
                    playingField,
                    points.get(i),
                    DOT_RADIUS
            );
            dot.setFill(Color.YELLOW);
            body.add(dot);
        }
        body.get(0).setFill(Color.BLUE);
    }
}