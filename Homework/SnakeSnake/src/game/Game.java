package game;

import ui.helpers.PlayerSettings;
import ui.helpers.SceneSetterHelper;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import model.Direction;
import socket.Client;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static model.Direction.*;

public class Game {

    private static final int WINDOW_WIDTH = 1280;
    private static final int WINDOW_HEIGHT = 720;
    private Client client;
    private Scene scene;
    private PlayingField playingField;
    private int player;
    private Thread connThread = new Thread(() -> {
        while (true) {
            String data = client.read();
            Scanner scanner = new Scanner(data);
            data = scanner.next();
            if ("/body".equals(data)) {
                int player = scanner.nextInt();
                List<Point> points = new ArrayList<>();
                while (scanner.hasNext()) {
                    points.add(new Point(
                            scanner.nextInt(),
                            scanner.nextInt()
                    ));
                }
                Platform.runLater(() -> {
                    playingField.setBody(player, points);
                });
            } else if ("/start".equals(data)) {
                start();
            } else if ("/food".equals(data)) {
                Platform.runLater(() -> {
                    playingField.addFood(new Point(scanner.nextInt(), scanner.nextInt()));
                });
            } else if ("/removeFood".equals(data)) {
                Platform.runLater(() -> {
                    playingField.removeFood(new Point(scanner.nextInt(), scanner.nextInt()));
                });
            } else if ("/finish".equals(data)) {
                Platform.runLater(() -> finish(scanner.nextInt()));
            }
        }
    });

    private void finish(int winner) {
        playingField.getChildren().clear();
        if (winner == player){
            onWin();
        } else lose();
            connThread.interrupt();
    }

    public void start(SceneSetterHelper sceneSetterHelper) {
        client = new Client();
        try {

            client.connect(PlayerSettings.getServerName(), 4444);
        } catch (IOException e) {
            e.printStackTrace();
        }
        player = Integer.parseInt(client.read());
        scene = new Scene(new Pane(), WINDOW_WIDTH, WINDOW_HEIGHT);
        scene.setOnKeyPressed(e -> {
            KeyCode key = e.getCode();
            Direction direction = null;
            if (key.equals(KeyCode.UP)) {
                direction = UP;
            } else if (key.equals(KeyCode.DOWN)) {
                direction = DOWN;
            } else if (key.equals(KeyCode.LEFT)) {
                direction = LEFT;
            }
            if (key.equals(KeyCode.RIGHT)) {
                direction = RIGHT;
            }
            if (direction != null) client.send("/changeDirection " + direction);
        });
        sceneSetterHelper.setScene(scene, "Snake war!");
        connThread.start();

    }

    public void start() {
        playingField = new PlayingField(WINDOW_WIDTH, WINDOW_HEIGHT);
        scene.setRoot(playingField);
    }


    private Node msgView(String msg) {
        Text text = new Text(0, WINDOW_HEIGHT / 2.0, msg);
        text.setWrappingWidth(WINDOW_WIDTH);
        text.setTextAlignment(TextAlignment.CENTER);

        return text;
    }


    public void onWin() {
        Node msg = generateMsgView("YOU WON");
        msg.setStyle("-fx-fill: green; -fx-font-size: 64");
        playingField.getChildren().add(msg);
    }

    public void lose() {
        Node msg = generateMsgView("YOU LOSE");
        msg.setStyle("-fx-fill: green; -fx-font-size: 64");
        playingField.getChildren().add(msg);
    }

    private Node generateMsgView(String message) {
        Text text = new Text(0, WINDOW_HEIGHT / 2, message);
        text.setTextAlignment(TextAlignment.CENTER);
        return text;
    }


}