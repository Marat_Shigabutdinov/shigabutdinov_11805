package game;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.Dot;
import model.Snake;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PlayingField extends Pane {

    private int width, height;
    private List<Snake> snakes = new ArrayList<>();
    private List<Dot> food = new ArrayList<>();


    public PlayingField(int width, int height) {
        this.width = width;
        this.height = height;
        snakes.add(new Snake(this));
        snakes.add(new Snake(this));
    }


    public void addFood(Point point) {
        Dot dot = new Dot(this, point);
        dot.setFill(Color.GREEN);
        food.add(dot);
    }


    public synchronized void setBody(int playerNumber, List<Point> body) {
        snakes.get(playerNumber).setBody(body);
    }

    public void removeFood(Point point) {
        for (Dot dot : food) {
            if (point.x == dot.getCenterX() && point.y == dot.getCenterY()) {
                food.remove(dot);
                getChildren().remove(dot);
                break;
            }
        }
    }
}